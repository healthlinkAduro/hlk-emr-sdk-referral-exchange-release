
## Introduction

This document specifies a SOAP interface that can be used for exchanging referrals and updates to referrals synchronously. It describes the web service and gives guidelines
on the implementation of the interface. 

## Interface overview 

The published documents can be found [here](https://bitbucket.org/healthlinkAduro/hlk-emr-sdk-referral-exchange-release/src/master/docs/). 

- The interface is for exchanging referrals but also for exchanging updates about those referrals. It can also deliver acknowledgment and negative acknowledgements about referral or referral update messages.
- The complete Referral Exchange interface is a symmetric interface in that both sides of the interface implement identical web services and corresponding web service clients. The web service interface consists of a *send* method as well as a version exchange *getVersion* method.
- It is possible to implement this asymmetrically and only implement the interface one way, in this situation acknowledgments would need to be returned in the web service response, and update messages back to the referrer would not be possible or require an alternate mechanism/interface. 

## Schema definitions

The WSDL and XSD files are in [this](https://bitbucket.org/healthlinkAduro/hlk-emr-sdk-referral-exchange-release/src/master/src/wsdl-xsd/) location

## Versioning
We use [SemVer](http://semver.org/) for versioning.

## Issues and support

For issues and questions, contact:

- [Roger Hill](roger.hill@healthlink.net)  
- [Chandan Datta](chandan.datta@healthlink.net)